import React from 'react';
import ReactDOM from 'react-dom';

import BasketProvider from '../providers/BasketProvider';
import { Global } from '../styles/global';
import Home from './Home';
import { Container } from './homeStyles';
import BasketPreview from '../components/BasketPreview/BasketPreview';
import { productsMock } from '../data/products';

const { products } = productsMock;

const Content = () => (
  <Container>
    <BasketProvider>
      <Global />
      <BasketPreview />
      <Home products={products} />
    </BasketProvider>
  </Container>
);

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Content name="React" />,
    document.body.appendChild(document.createElement('div')),
  );
});
