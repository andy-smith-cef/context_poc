import React from 'react';
import ReactDOM from 'react-dom';

import BasketProvider from '../providers/BasketProvider';
import { Global } from '../styles/global';
import Basket from '../components/Basket/Basket';
import { Container } from './homeStyles';
import BasketPreview from '../components/BasketPreview/BasketPreview';

const Content = () => (
  <Container>
    <BasketProvider>
      <Global />
      <BasketPreview />
      <Basket />
    </BasketProvider>
  </Container>
);

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Content name="React" />,
    document.body.appendChild(document.createElement('div')),
  );
});
