import React from 'react';

import styled from 'styled-components';
import RenderProduct from '../components/RenderProduct';
import { useBasketContext } from '../providers/BasketProvider';

const Home = ({ products }) => {
  const { prices, loadingPrice } = useBasketContext();
  return (
    <>
      <p>Products</p>
      <ResultsContainer>
        <Aggrigations>
          <p>test</p>
        </Aggrigations>
        <Results>
          {products.map((prod) => (
            <RenderProduct key={prod.id} prod={prod} prices={prices} loadingPrice={loadingPrice} />
          ))}
        </Results>
      </ResultsContainer>
    </>
  );
};
export default Home;

export const ResultsContainer = styled.div`
  flex: 1;
  display: flex;
`;

export const Aggrigations = styled.div`
  width: 23.07692%;
  background-color: gray;
`;

export const Results = styled.div`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  margin-left: 2.5641%;
`;
