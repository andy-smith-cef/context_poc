import Axios from 'axios';

const getBasketFromUrl = async (url, customerId, basketLines) => {
  // const body = {
  //   customer_id: customerId,
  //   basket_lines: basketLines,
  // };

  // return Axios.post(url, body, {
  //   withCredentials: 'true',
  //   headers: {
  //     Accept: 'application/json',
  //     'Content-Type': 'application/json',
  //   },
  // }).then((response) => {
  //   const { data } = response;
  //   const { basket } = data;
  //   return basket;
  // });

  console.log('getting basket ---------');
};

export const validateBasket = async (stateBasket, customerId, basketLinesResponse) => {
  try {
    // const {
    //   lines,
    //   removed_products: removedProducts,
    //   total_excluding_vat: subTotal,
    //   total_including_vat: total,
    //   vat,
    // } = await getBasketFromUrl(url, customerId, basketLinesResponse);

    let removed = [];
    let newBasket = stateBasket;

    // if (removedProducts.length > 0) {
    //   const getRemovedLines = (original, remove) => {
    //     return original.filter((line) => remove.includes(line.product_id));
    //   };

    //   const removeFromArray = (original, remove) => {
    //     return original.filter((value) => !remove.includes(value.product_id));
    //   };

    //   removed = getRemovedLines(stateBasket, removedProducts);
    //   newBasket = removeFromArray(stateBasket, removedProducts);
    // }

    const basket = newBasket.map((line) => {
      const item = lines.filter((l) => l.product_id === line.product_id);
      if (item.length) {
        const { minimum_order: minimumOrder, maximum_quantity: maximumQuantity } = line.product;

        line.message = item[0].message;

        if (
          line.quantity % minimumOrder !== 0 ||
          line.quantity < item[0].quantity ||
          line.quantity > maximumQuantity
        ) {
          line.quantity = item[0].quantity;
        }
      }

      return line;
    });

    return { basket, removed, subTotal, total, vat };
  } catch (e) {
    return false;
  }
};
