import React from 'react';
import Svg, { Path } from 'react-native-svg';

const ErrorIcon = ({ width, height }) => (
  <Svg width={width} height={height}>
    <Path
      data-name="Path 10708"
      d="M10 0L0 17.316h20zm1.386 7.286l-.295 4.366H8.879l-.295-4.366zm-.236 7.64H8.879v-2.183h2.271z"
      fill="#d41f3a"
    />
  </Svg>
);

export default ErrorIcon;
