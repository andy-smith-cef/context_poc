import React from 'react';
import Svg, { Path } from 'react-native-svg';

const MailIcon = () => (
  <Svg width={21} height={14.257} viewBox="0 0 21 14.257">
    <Path
      data-name="Path 10718"
      d="M0 0v14.257h21V0zm19.957 1L10.5 7.678 1.044 1zM1 13.257V2.193L10.5 8.9 20 2.193v11.064z"
      fill="#fff"
    />
  </Svg>
);

export default MailIcon;
