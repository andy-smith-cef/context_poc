import React from 'react';
import Svg, { Path } from 'react-native-svg';

const ModalInfoIcon = () => (
  <Svg width={25} height={25} viewBox="0 0 25 25">
    <Path
      data-name="Path 10527"
      d="M12.5 0A12.5 12.5 0 1025 12.5 12.5 12.5 0 0012.5 0zm-1.808 4.422h3.632v3.12h-3.632zm5.478 16.156H8.83v-3.212h1.892v-4.657H8.83V9.5h5.494v7.869h1.846z"
      fill="#d41f3a"
    />
  </Svg>
);

export default ModalInfoIcon;
