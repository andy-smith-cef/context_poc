import React from 'react';
import Svg, { Path } from 'react-native-svg';

const HeaderBackAndroid = () => (
  <Svg width={17} height={17} viewBox="0 0 17 17" fill="none">
    <Path fill="#fff" d="M0 8.462L8.485 0l1.417 1.412-8.486 8.463z" />
    <Path fill="#fff" d="M8.485 17L0 8.537l1.416-1.412 8.485 8.463z" />
    <Path fill="#fff" d="M2.5 7.5H17v2H2.5z" />
  </Svg>
);

export default HeaderBackAndroid;
