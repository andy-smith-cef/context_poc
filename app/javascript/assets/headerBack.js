import React from 'react';
import Svg, { Rect } from 'react-native-svg';

const HeaderBack = () => (
  <Svg width={15} height={23} viewBox="0 0 15 23" fill="none">
    <Rect
      width={16}
      height={3}
      rx={1.5}
      transform="matrix(.7071 -.7071 .67049 .74192 .883 11.43)"
      fill="#fff"
    />
    <Rect
      width={16}
      height={3}
      rx={1.5}
      transform="matrix(-.7071 -.7071 .67049 -.74192 12.197 22.93)"
      fill="#fff"
    />
  </Svg>
);

export default HeaderBack;
