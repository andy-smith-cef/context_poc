import { useAsync } from 'react-async-hook';
import useConstant from 'use-constant';
import AwesomeDebouncePromise from 'awesome-debounce-promise';
import Axios from 'axios';
import { useState } from 'react';

import { productsMock } from '../data/productPrices';

const getProductPrices = (newProducts, productPriceUrl, singlePriceCheck) => {
  if (Object.keys(newProducts).length) {
    const data = {
      success: true,
      data: productsMock,
    };
    return data;
    // let singleUrl = `${productPriceUrl}/show`;
    // Object.keys(newProducts).forEach((product, i) => {
    //   singleUrl =
    //     i === 0
    //       ? `${singleUrl}?products[${product}]=${newProducts[product]}`
    //       : `${singleUrl}&products[${product}]=${newProducts[product]}`;
    // });
    // const priceUrlSwitch = singlePriceCheck
    //   ? singleUrl
    //   : `${productPriceUrl}?products=${Object.keys(newProducts[0]).join(',')}`;
    // return Axios.get(priceUrlSwitch, {
    //   headers: {
    //     Accept: 'application/javascript',
    //     'Content-Type': 'application/javascript',
    //   },
    // })
    //   .then(({ data }) => ({
    //     success: true,
    //     data,
    //   }))
    //   .catch(({ response }) => ({ success: false, data: response }));
  }
  return undefined;
};

const usePricing = () => {
  const [productsForPricing, setProductsForPricing] = useState({});
  const [productPriceUrl, setProductPriceUrl] = useState('');
  const [singlePriceCheck, setSinglePriceCheck] = useState(false);

  const debounceGetProductPrices = useConstant(() => AwesomeDebouncePromise(getProductPrices, 100));

  const productPrices = useAsync(
    async () => debounceGetProductPrices(productsForPricing, productPriceUrl, singlePriceCheck),
    [productsForPricing, productPriceUrl, singlePriceCheck],
  );

  return {
    productPrices,
    setProductPriceUrl,
    setProductsForPricing,
    setSinglePriceCheck,
  };
};

export default usePricing;
