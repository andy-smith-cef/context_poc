import Colors from '../constants/Colors';

// export const l = StyleSheet.create({
//   absolute: {
//     position: 'absolute',
//   },
//   alignBaseline: {
//     alignItems: 'baseline',
//   },
//   bgWhite: {
//     backgroundColor: '#fff',
//   },
//   bold: {
//     fontWeight: 'bold',
//   },
//   flex1: {
//     flex: 1,
//   },
//   flex3: {
//     flex: 3,
//   },
//   flexRow: {
//     flexDirection: 'row',
//   },
//   flexCol: {
//     flexDirection: 'column',
//   },
//   flexGrow1: {
//     flexGrow: 1,
//   },
//   noWrap: {
//     flexWrap: 'nowrap',
//   },
//   wrap: {
//     flexWrap: 'wrap',
//   },
//   textCenter: {
//     textAlign: 'center',
//   },
//   itemsCenter: {
//     alignItems: 'center',
//   },
//   itemsEnd: {
//     alignItems: 'flex-end',
//   },
//   justifyBetween: {
//     justifyContent: 'space-between',
//   },
//   justifyCenter: {
//     justifyContent: 'center',
//   },
//   justifyEnd: {
//     justifyContent: 'flex-end',
//   },
//   m0: {
//     margin: 0,
//   },
//   m20: {
//     margin: 20,
//   },
//   mb1: {
//     marginBottom: 1,
//   },
//   mb4: {
//     marginBottom: 4,
//   },
//   mb8: {
//     marginBottom: 8,
//   },
//   mb10: {
//     marginBottom: 10,
//   },
//   mb16: {
//     marginBottom: 16,
//   },
//   mb20: {
//     marginBottom: 20,
//   },
//   mb24: {
//     marginBottom: 24,
//   },
//   mb32: {
//     marginBottom: 32,
//   },
//   mb50: {
//     marginBottom: 50,
//   },
//   mb100: {
//     marginBottom: 100,
//   },
//   mh8: {
//     marginHorizontal: 8,
//   },
//   mh16: {
//     marginHorizontal: 16,
//   },
//   ml4: {
//     marginLeft: 4,
//   },
//   ml6: {
//     marginLeft: 6,
//   },
//   ml8: {
//     marginLeft: 8,
//   },
//   ml10: {
//     marginLeft: 10,
//   },
//   ml16: {
//     marginLeft: 16,
//   },
//   ml20: {
//     marginLeft: 20,
//   },
//   ml32: {
//     marginLeft: 32,
//   },
//   mr4: {
//     marginRight: 4,
//   },
//   mr6: {
//     marginRight: 6,
//   },
//   mr8: {
//     marginRight: 8,
//   },
//   mr10: {
//     marginRight: 10,
//   },
//   mr14: {
//     marginRight: 14,
//   },
//   mr16: {
//     marginRight: 16,
//   },
//   mt4: {
//     marginTop: 4,
//   },
//   mt8: {
//     marginTop: 8,
//   },
//   mt10: {
//     marginTop: 10,
//   },
//   mt12: {
//     marginTop: 12,
//   },
//   mt16: {
//     marginTop: 16,
//   },
//   mt20: {
//     marginTop: 20,
//   },
//   mt32: {
//     marginTop: 32,
//   },
//   mt40: {
//     marginTop: 40,
//   },
//   mt48: {
//     marginTop: 48,
//   },
//   mv10: {
//     marginVertical: 10,
//   },
//   mv20: {
//     marginVertical: 20,
//   },
//   p0: {
//     padding: 0,
//   },
//   p8: {
//     padding: 8,
//   },
//   p10: {
//     padding: 10,
//   },
//   p16: {
//     padding: 16,
//   },
//   ph4: {
//     paddingHorizontal: 4,
//   },
//   ph8: {
//     paddingHorizontal: 8,
//   },
//   ph10: {
//     paddingHorizontal: 10,
//   },
//   ph12: {
//     paddingHorizontal: 12,
//   },
//   ph16: {
//     paddingHorizontal: 16,
//   },
//   ph20: {
//     paddingHorizontal: 20,
//   },
//   pl10: {
//     paddingLeft: 10,
//   },
//   pl16: {
//     paddingLeft: 16,
//   },
//   pt10: {
//     paddingTop: 10,
//   },
//   pt20: {
//     paddingTop: 20,
//   },
//   pv8: {
//     paddingVertical: 8,
//   },
//   pv10: {
//     paddingVertical: 10,
//   },
//   pv12: {
//     paddingVertical: 12,
//   },
//   pv16: {
//     paddingVertical: 16,
//   },
//   pr4: {
//     paddingRight: 4,
//   },
//   pr8: {
//     paddingRight: 8,
//   },
//   pr10: {
//     paddingRight: 10,
//   },
//   pr16: {
//     paddingRight: 16,
//   },
//   pb1: {
//     paddingBottom: 1,
//   },
//   pb4: {
//     paddingBottom: 4,
//   },
//   pb16: {
//     paddingBottom: 16,
//   },
//   pb20: {
//     paddingBottom: 20,
//   },
//   selfStart: {
//     alignSelf: 'flex-start',
//   },
//   selfEnd: {
//     alignSelf: 'flex-end',
//   },
//   selfCentered: {
//     alignSelf: 'center',
//   },
//   optionalText: {
//     color: Colors.lightGrey,
//     fontSize: 14,
//   },
//   fs16: {
//     fontSize: 16,
//   },
//   fs18: {
//     fontSize: 18,
//   },
//   fs20: {
//     fontSize: 20,
//   },
//   borderBottomLight: {
//     borderBottomWidth: 1,
//     borderBottomColor: Colors.borderColor,
//   },
//   centerText: { textAlign: 'center' },
// });
