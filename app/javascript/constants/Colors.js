const cefRed = '#D41F3A';
const cefRedInactive = '#F4D7D7';
const black = '#000000';
const white = '#FFFFFF';
const fontMain = '#4B4B4B'; // cant think of better name
const fontLight = '#4A4F51';
const offWhite = '#F2F2F2';
const success = '#207600';
const lightGrey = '#7E7E7E';
const borderColor = '#E4E4E4';
const secondaryBorderColor = '#B1B1B1';
const headingColor = '#494F52';
const paypalGold = '#ffc439';
const placeholderTextColor = '#7F7F7F';

export default {
  cefRed,
  cefRedInactive,
  white,
  black,
  fontMain,
  fontLight,
  offWhite,
  success,
  iconInactive: '#9c9c9c',
  lightGrey,
  borderColor,
  secondaryBorderColor,
  headingColor,
  paypalGold,
  placeholderTextColor,
};
