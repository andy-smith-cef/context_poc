import analytics from '@react-native-firebase/analytics';
import Config from 'react-native-config';
const logAnalyticsEvent = (name, properties = {}) => {
  const { ENVIRONMENT } = Config;
  ENVIRONMENT !== 'development' && analytics().logEvent(name, properties);
};

export const setAnalyticsScreen = screenName => {
  const { ENVIRONMENT } = Config;
  ENVIRONMENT !== 'development' &&
    analytics().logScreenView({
      screen_class: screenName,
      screen_name: screenName,
    });
};

export const setAnalyticsUserProperty = (property, type) => {
  const { ENVIRONMENT } = Config;
  ENVIRONMENT !== 'development' && analytics().setUserProperty(property, type);
};

export const logAnalyticsPurchase = properties => {
  const { ENVIRONMENT } = Config;

  ENVIRONMENT !== 'development' && analytics().logPurchase(properties);
};

export default logAnalyticsEvent;
