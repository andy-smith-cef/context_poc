import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import Colors from '../../constants/Colors';
import { useBasketContext } from '../../providers/BasketProvider';
import LoadingSpinner from './loadingSpinner';

const PriceHelper = ({ loadingPrice, price, defaultPriceIncVat, defaultMinimumOrder, id }) => {
  const {
    price_inclusive: priceIncVat,
    quantity: minimumOrder,
    unit: unit,
    price: priceExVat,
  } = price;

  const { incVAT, VATToggleDisabled } = useBasketContext();

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (loadingPrice) {
      const loadingIds = Object.keys(loadingPrice);
      loadingIds.forEach((loadingIdPrice) => {
        if (parseInt(loadingIdPrice, 10) === id) {
          setIsLoading(true);
        }
      });
    } else {
      setIsLoading(false);
    }
  }, [loadingPrice]);

  const handleUnit = (parsedUnit) => {
    let value;

    switch (parsedUnit) {
      case 'm':
      case 'kg':
        value = parsedUnit;
        break;
      case 'each':
      case 'Each':
        value = '';
        break;
      case null:
        value = '';
        break;
      default:
        value = ` ${parsedUnit}`;
    }

    return value;
  };

  if (isLoading) {
    return (
      <LoadingContainer>
        <LoadingSpinner size={'small'} color={'#d02239'} />
      </LoadingContainer>
    );
  }
  return (
    <>
      <PriceContainer>
        <PriceContainerTop>
          <PriceBox>
            &pound;
            {incVAT && !VATToggleDisabled
              ? priceIncVat
                ? priceIncVat.toFixed(2)
                : defaultPriceIncVat.toFixed(2)
              : priceExVat
              ? priceExVat.toFixed(2)
              : 0}
          </PriceBox>
          <PriceBoxSmall>
            {` For ${minimumOrder ? minimumOrder : defaultMinimumOrder}${
              unit !== null ? handleUnit(unit) : ''
            } `}
          </PriceBoxSmall>
        </PriceContainerTop>

        <PriceBoxSmall>
          {`£${
            incVAT && !VATToggleDisabled
              ? priceExVat
                ? priceExVat.toFixed(2)
                : defaultPriceExVat.toFixed(2)
              : priceIncVat
              ? priceIncVat.toFixed(2)
              : 0
          }${' '}${incVAT && !VATToggleDisabled ? 'ex' : 'inc'} VAT`}
        </PriceBoxSmall>
      </PriceContainer>
    </>
  );
};

PriceHelper.defaultProps = {
  price: {
    price_inclusive: null,
    quantity: null,
    unit: null,
    price: null,
    breaks: null,
    per_pack: null,
  },
};

export default PriceHelper;

const LoadingContainer = styled.div`
  align-items: flex-start;
  align-content: flex-start;
`;

const PriceContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  font-family: sans-serif;
`;

const PriceContainerTop = styled.div`
  flex-direction: row;
  align-items: baseline;
  flex-wrap: wrap;
  display: flex;
`;

const PriceBox = styled.p`
  color: #000;
  font-size: 1.5em;
  background: none;
  padding: 0;
  font-weight: 600;
  margin-top: 0;
  line-height: 1.1em;
`;

const PriceBoxSmall = styled.p`
  font-size: 12px;
  color: ${Colors.fontMain};
  font-weight: '300';
`;
