import React from 'react';
import { Image } from 'react-native';

const NoImage = ({ height, width }) => (
  <Image
    source={{
      uri:
        'https://pdg.cef.co.uk/images/pdg/no_image/${height}x${width}/no_image.png',
    }}
    style={{
      height,
      width,
    }}
  />
);

export default NoImage;
