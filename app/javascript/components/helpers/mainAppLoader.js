import React from 'react';
import { StyleSheet, View } from 'react-native';
import LoadingSpinner from './loadingSpinner';
import Logo from '../../screens/assets/Logo';

const MainAppLoader = () => (
  <View style={styles.viewStyles}>
    <Logo height={90} width={205} />
    <LoadingSpinner size={'large'} color={'#B81D32'} />
  </View>
);

export default MainAppLoader;

const styles = StyleSheet.create({
  viewStyles: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
