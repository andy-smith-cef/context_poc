import React from 'react';

import { CefText } from '../styles/Typography';

const LoadingSpinner = ({ size, color, message }) => {
  return (
    <>
      LOADING...
      {message && <CefText>{message}</CefText>}
    </>
  );
};

export default LoadingSpinner;
