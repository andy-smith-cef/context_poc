import styled from 'styled-components/native';
import Colors from '../../../constants/Colors';
import { spacing } from '../styleConfig';

export const ScreenContainer = styled.View`
  flex: 1;
  padding: ${spacing}px;
  background-color: ${({ color, grey }) =>
    grey ? Colors.borderColor : color ? color : Colors.white};
`;
