import styled from 'styled-components';
import Colors from '../../constants/Colors';
import { spacing } from './styleConfig';

// Families
export const FamilyMain = 'sans-serif';
export const FamilyAlt = 'sans-serif-condensed';

// Sizes
export const fontSizeMassive = 24;
export const fontSizeLarge = 18;
export const fontSizeStandard = 16;
export const fontSizeSmall = 14;
export const fontSizeTiny = 12;
export const fontSizeSmallest = 11;

// Types
export const CefText = styled.div`
  font-family: ${FamilyMain};
  color: ${({ color }) => color || Colors.fontMain};
  text-align: ${({ taLeft, taCenter }) => (taLeft ? 'left' : taCenter ? 'center' : 'left')};
  font-size: ${({ fsLarge, fsSmall }) =>
    fsLarge ? fontSizeStandard : fsSmall ? fontSizeTiny : fontSizeSmall}px;
  font-weight: ${({ bold, light }) => (bold ? 'bold' : light ? '300' : 'normal')};
  margin-top: ${({ mt }) => (mt ? mt : 0)}px;
  margin-bottom: ${({ mb }) => (mb ? mb : 0)}px;
  margin-left: ${({ ml }) => (ml ? ml : 0)}px;
  margin-right: ${({ mr }) => (mr ? mr : 0)}px;
  ${({ flex }) => (flex ? 'flex: 1' : '')}
  opacity: ${({ opacity }) => (opacity ? opacity : 1)}
`;

export const InputLabel = styled(CefText)`
  text-align: left;
  color: ${({ color }) => (color ? color : Colors.fontMain)};
  margin-bottom: ${({ marginBottom }) => (marginBottom ? spacing / 2 : 0)}px;
  margin-top: ${({ marginTop }) => (marginTop ? 20 : 0)}px;
  margin-left: ${({ marginLeft }) => (marginLeft ? 4 : 0)}px;
  padding-left: ${({ paddingLeft }) => (paddingLeft ? 4 : 0)}px;
  font-weight: ${({ bold, light }) => (bold ? 'bold' : light ? '300' : 'normal')};
`;

// Headings
export const CondensedHeading = styled(CefText)`
  font-family: ${FamilyAlt};
  font-size: ${({ fs }) => (fs ? fs : fontSizeLarge)}px;
  font-weight: ${({ fwNormal }) => (fwNormal ? 'normal' : 'bold')};
  text-align: ${({ taCenter }) => (taCenter ? 'center' : 'left')};
  margin-bottom: ${({ mb }) => (mb ? mb : 0)}px;
`;

// Buttons
export const ButtonText = styled(CefText)`
  font-size: ${fontSizeStandard}px;
  font-weight: bold;
  text-align: center;
  color: ${({ color }) => (color ? color : Colors.white)};
`;
export const ButtonSecondaryText = styled(ButtonText)`
  color: ${Colors.fontMain};
`;
