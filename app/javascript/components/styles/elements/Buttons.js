import React from 'react';

import styled from 'styled-components/native';
import Colors from '../../../constants/Colors';
import { spacing } from '../styleConfig';
import { ButtonText } from '../Typography';

export const Button = styled.TouchableOpacity`
  background-color: ${({ secondary, borderButton, disabled, whiteButton }) =>
    secondary
      ? Colors.fontMain
      : borderButton
      ? 'transparent'
      : disabled
      ? Colors.cefRedInactive
      : whiteButton
      ? Colors.white
      : Colors.cefRed};
  height: ${({ height }) => (height ? height : 47)}px;
  width: ${({ width }) => (width ? width : 100)}%;
  margin-top: ${({ mt }) => (typeof mt !== 'undefined' ? mt : spacing)}px;
  margin-left: ${({ ml }) => (ml ? ml : 0)}px;
  margin-right: ${({ mr }) => (mr ? mr : 0)}px;
  margin-bottom: ${({ mb }) => (mb ? mb : spacing / 4)}px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  ${({ borderButton }) =>
    borderButton
      ? {
          borderWidth: 2,
          borderColor: Colors.fontMain,
        }
      : ''}
  ${({ flex }) => (flex ? { flex: 1 } : '')};
`;

export const CefButton = ({
  onPress,
  text,
  secondary,
  borderButton,
  icon,
  ml,
  mr,
  mt,
  mb,
  height,
  columns,
  flex,
  disabled,
  whiteButton,
}) => (
  <Button
    onPress={onPress}
    disabled={disabled}
    secondary={secondary}
    borderButton={borderButton}
    whiteButton={whiteButton}
    ml={ml}
    mr={mr}
    mt={mt}
    mb={mb}
    height={height}
    width={columns ? (1 / columns) * 100 : 100}
    flex={flex}>
    {icon && icon}
    <ButtonText
      ml={icon ? spacing : 0}
      color={
        borderButton
          ? Colors.fontMain
          : whiteButton
          ? Colors.cefRed
          : Colors.white
      }>
      {text}
    </ButtonText>
  </Button>
);
