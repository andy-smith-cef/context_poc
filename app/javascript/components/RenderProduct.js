import React, { memo } from 'react';
import Product from './Product';

const RenderProduct = ({ prod, prices, loadingPrice }) => {
  const {
    b2b_stock_level: stock,
    catalogue,
    brand_and_description_short: description,
    id,
    image_medium: image,
    stock_code: stockCode,
  } = prod;

  const price =
    prices && Object.entries(prices).length === 0 && prices.constructor === Object
      ? false
      : prices[id];

  return (
    <Product
      stock={stock}
      catalogue={catalogue}
      description={description}
      id={id}
      image={image}
      item={prod}
      key={id}
      stockCode={stockCode}
      price={price}
      loadingPrice={loadingPrice}
    />
  );
};

export default memo(RenderProduct);
