import React from 'react';
import styled from 'styled-components';

import AddToBasket from './AddToBasket/AddToBasket';
import PriceHelper from './helpers/priceHelper';
import { CefText } from './styles/Typography';
import Colors from '../constants/Colors';
import { spacing } from './styles/styleConfig';

const Product = ({
  stock,
  catalogue,
  description,
  id,
  image,
  item,
  stockCode,
  loadingPrice,
  price,
}) => (
  <ProductBox>
    <ProductBoxTop>
      <ProductBoxImageWrapper>
        <ProductBoxImage src={image} alt="" />
      </ProductBoxImageWrapper>

      <ProductBoxTopContent>
        <div>
          <CefText color={Colors.black} mb={spacing}>
            {description}
          </CefText>
        </div>
        <ProductBoxCodes>
          <CefText light fsSmall mr={spacing} mb={spacing}>
            <strong>Stock Code:</strong> {stockCode}
          </CefText>
          <CefText light fsSmall mr={spacing} mb={spacing}>
            <strong>Part Code:</strong> {catalogue}
          </CefText>
        </ProductBoxCodes>
        <PriceHelper
          id={id}
          loadingPrice={loadingPrice}
          defaultPriceIncVat={item.price_inclusive_vat}
          defaultMinimumOrder={item.minimum_order}
          defaultPriceExVat={item.price_exclusive_vat}
          price={price}
          hideBreaks={false}
        />
      </ProductBoxTopContent>
    </ProductBoxTop>
    <ProductBoxBottom>
      <ProductBoxBottomStock>
        <ProductBoxBottomStockValue>
          <CefText color={Colors.black}>
            <strong>Delivery:&nbsp;</strong>
          </CefText>
          <CefText color={Colors.success} bold>
            {stock} in Stock
          </CefText>
        </ProductBoxBottomStockValue>
        <ProductBoxBottomStockValue>
          <CefText color={Colors.black}>
            <strong>Collection:&nbsp;</strong>
          </CefText>
          <CefText color={Colors.cefRed} bold>
            Select a Store
          </CefText>
        </ProductBoxBottomStockValue>
      </ProductBoxBottomStock>
      <ProductBoxAddToBasketWrapper>
        <AddToBasket
          id={id}
          screen="Product List"
          height={30}
          buttonText="Add to Basket"
          product={item}
        />
      </ProductBoxAddToBasketWrapper>
    </ProductBoxBottom>
  </ProductBox>
);

export default Product;

export const ProductBox = styled.div`
  border: 1px solid #d1d1d1;
  padding: 1.7%;
  margin: 15px;
  display: flex;
  flex-direction: column;
  flex-basis: 30%;
`;

export const ProductBoxTop = styled.div`
  flex-direction: column;
  display: flex;
  align-items: center;
`;

export const ProductBoxTopContent = styled.div`
  flex: 1;
  display: flex;
  margin-left: 20;
  flex-direction: column;
`;

export const ProductBoxImageWrapper = styled.div`
  margin-bottom: 1.25rem;
`;

export const ProductBoxImage = styled.img`
  min-height: 100%;
  max-width: 210px;
`;

export const ProductBoxCodes = styled.div`
  flex-direction: column;
  display: flex;
`;

export const ProductBoxBottom = styled.div`
  flex-direction: column;
  display: flex;
`;

export const ProductBoxBottomStock = styled.div`
  flex-direction: row;
  display: flex;
  align-items: 'center';
  justify-content: 'space-between';
`;

export const ProductBoxBottomStockValue = styled.div`
  background-color: #edf1f3;
  border: 0;
  box-sizing: border-box;
  flex-basis: calc(50% - 1.5rem);
  flex-grow: 1;
  flex-shrink: 1;
  height: 55px;
  padding: 0 0.625rem;
  flex-direction: column;
  display: flex;
  justify-content: center;

  &:nth-child(2) {
    margin-left: 0.625rem;
  }
`;

export const ProductBoxAddToBasketWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;
