import React, { useState, useRef } from 'react';
import styled from 'styled-components';

import { asyncLocalStorage } from '../../utils/awaitAsyncLocalStorage';
import { useBasketContext } from '../../providers/BasketProvider';
// import { l } from '../../styles/Layout';
import Colors from '../../constants/Colors';
import PlusIcon from './assets/PlusIcon';
import MinusIcon from './assets/MinusIcon';
// import useModal from '../../hooks/useModal';
import { ButtonText } from '../styles/Typography';
import { getBasket } from '../../utils/common';

const AddToBasket = ({ id, buttonText, hideSelector, product }) => {
  const {
    minimum_order: minimumOrder,
    delivery_minimum_order: deliveryMinOrder,
    maximum_quantity: maxQuantity,
    b2b_stock_level: stock,
    description_short,
  } = product;

  const [disabled, setDisabled] = useState(false);
  const [isAdded, setIsAdded] = useState(false);
  const [quantity, setQuantity] = useState(deliveryMinOrder);

  const {
    handleBasketValidation,
    setProductsForPricingContext,
    setSinglePriceCheck,
    setLoadingPrice,
  } = useBasketContext();

  const inputRef = useRef();

  const disableButtonState = async () => setIsAdded(true);
  const resetButtonState = () => setTimeout(() => setIsAdded(false), 2000);
  const incrementQuantity = () => {
    setQuantity(+quantity + minimumOrder);
    //setSinglePriceCheck(true);

    const productPriceParams = {
      [id]: +quantity + minimumOrder,
    };
    setLoadingPrice(productPriceParams);
    setProductsForPricingContext(productPriceParams);
  };
  const decrementQuantity = () => {
    setQuantity(+quantity === deliveryMinOrder ? deliveryMinOrder : +quantity - minimumOrder);

    const productPriceParams = {
      [id]: +quantity === deliveryMinOrder ? deliveryMinOrder : +quantity - minimumOrder,
    };
    setLoadingPrice(productPriceParams);
    // setSinglePriceCheck(productPriceParams);
    setProductsForPricingContext(productPriceParams);
  };

  const validateQuantity = () => {
    setDisabled(false);
    if (+quantity < +deliveryMinOrder && +quantity <= minimumOrder) {
      setQuantity(deliveryMinOrder);

      const productPriceParams = {
        [id]: deliveryMinOrder,
      };
      setLoadingPrice(productPriceParams);
      // setSinglePriceCheck(true);
      setProductsForPricingContext(productPriceParams);
    } else if (+quantity % minimumOrder !== 0 || +quantity <= 0 || +quantity < +deliveryMinOrder) {
      setQuantity(deliveryMinOrder);

      const productPriceParams = {
        [id]: deliveryMinOrder,
      };
      setLoadingPrice(productPriceParams);
      // setSinglePriceCheck(true);
      setProductsForPricingContext(productPriceParams);
    } else if (+quantity > +stock && +stock !== 0) {
      setIsAdded(false);
    } else if (+quantity > maxQuantity) {
      setQuantity(maxQuantity);

      const productPriceParams = {
        [id]: maxQuantity,
      };
      setLoadingPrice(productPriceParams);
      // setSinglePriceCheck(true);
      setProductsForPricingContext(productPriceParams);
    } else {
      const productPriceParams = {
        [id]: quantity,
      };
      setLoadingPrice(productPriceParams);
      // setSinglePriceCheck(true);
      setProductsForPricingContext(productPriceParams);
    }
  };

  const updateBasket = async (id, product) => {
    let basketString = await getBasket();

    let parsedBasket = JSON.parse(basketString);

    if (basketString !== 'null') {
      const index = parsedBasket.findIndex((b) => b.product_id === id);
      if (index === -1) {
        parsedBasket.push({ product_id: id, quantity, product });
        asyncLocalStorage.setItem('@basket', JSON.stringify(parsedBasket));
        handleBasketValidation(parsedBasket);
      } else {
        parsedBasket[index].quantity = parsedBasket[index].quantity + quantity;
        asyncLocalStorage.setItem('@basket', JSON.stringify(parsedBasket));
        handleBasketValidation(parsedBasket);
      }
    } else {
      asyncLocalStorage.setItem('@basket', JSON.stringify([{ product_id: id, quantity, product }]));
      basketString = await getBasket();

      parsedBasket = JSON.parse(basketString);
      handleBasketValidation(parsedBasket);
    }
  };

  return (
    <>
      {!hideSelector && (
        <QuantitySelector>
          <MinusQuantity onClick={() => decrementQuantity()}>
            <MinusIcon
              width={10}
              height={2}
              fill={quantity !== 1 ? Colors.cefRed : '#b2b2b2'}
              stroke={quantity !== 1 ? Colors.cefRed : '#b2b2b2'}
            />
          </MinusQuantity>
          <QuantityInput
            ref={inputRef}
            onChange={(val) => setQuantity(val)}
            onBlur={() => validateQuantity()}
            onFocus={() => setDisabled(true)}
            value={quantity.toString()}
          />
          <PlusQuantity onClick={() => incrementQuantity()}>
            <PlusIcon width={10} height={10} stroke={Colors.cefRed} />
          </PlusQuantity>
        </QuantitySelector>
      )}
      <AddToBasketButton
        disabled={isAdded || stock === 0}
        onClick={() => {
          if (disabled) {
            inputRef.current.blur();
          } else {
            disableButtonState()
              .then(() => {
                const { b2b_stock_level: stock } = product;

                if (stock === 0) {
                  setIsAdded(false);

                  console.error('Product out of stock');
                  // updateModalContent({
                  //   title: 'Product out of stock',
                  //   subTitle: 'This item will be placed on backorder and dispatched when in stock',
                  //   type: 'noStock',
                  //   addToBasket: {
                  //     id: Number(id),
                  //     quantity: Number(quantity),
                  //     product,
                  //   },
                  // });
                } else if (+quantity > +stock && +stock !== 0) {
                  setIsAdded(false);
                  console.error('Insufficient Stock');
                  // updateModalContent({
                  //   title: 'Insufficient Stock',
                  //   subTitle: `Sorry, we only have ${stock} in stock and therefore the balance will be placed on back order`,
                  //   type: 'noStock',
                  //   addToBasket: {
                  //     id: Number(id),
                  //     quantity: Number(quantity),
                  //     product,
                  //   },
                  // });
                } else {
                  updateBasket(Number(id), product).then(setTimeout(() => setIsAdded(false), 2000));
                }
              })
              .then(resetButtonState());
          }
        }}>
        <ButtonText>{isAdded ? 'Added' : buttonText || 'Add to Basket'}</ButtonText>
      </AddToBasketButton>
    </>
  );
};

export default AddToBasket;

const QuantitySelector = styled.div`
  align-items: center;
  border: 1px solid #b9b9b9;
  display: flex;
  font-size: 1.3em;
  justify-content: space-between;
  margin: 20px 0;
`;

const MinusQuantity = styled.button`
  background: none;
  border: none;
  box-shadow: none;
  height: 40px;
  padding: 0;
  width: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const PlusQuantity = styled.button`
  background: none;
  border: none;
  box-shadow: none;
  height: 40px;
  padding: 0;
  width: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const QuantityInput = styled.input`
  border: none;
  box-shadow: none;
  font-size: 1em;
  font-weight: 200;
  padding: 0;
  text-align: center;
  width: 90px;
`;

const AddToBasketButton = styled.button`
  font-size: 14px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #207600;
  color: #fff;
  height: 40px;
  border: 0;
  border-radius: 0;

  &:disabled {
    opacity: 0.5;
    background-color: ${Colors.fontMain};
  }
`;

// const styles = StyleSheet.create({
//   addToBasketInputWrapper: {
//     backgroundColor: Colors.white,
//     borderColor: Colors.secondaryBorderColor,
//     borderWidth: 1,
//     width: 95,
//   },
//   quantityInput: {
//     height: 40,
//   },
//   textInput: {
//     borderWidth: 0,
//     color: Colors.fontMain,
//     textAlign: 'center',
//   },
//   addToBasketButton: {
//     backgroundColor: Colors.cefRed,
//     height: 40,
//   },
//   addToBasketButtonDisabled: {
//     backgroundColor: Colors.success,
//   },
// });
