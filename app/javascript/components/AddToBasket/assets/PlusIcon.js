import React from 'react';

const PlusIcon = ({ width, height, stroke }) => (
  <svg width={width} height={height} viewBox="0 0 14 14" stroke={stroke}>
    <g data-name="Group 15106" fill="none" strokeWidth={2}>
      <path data-name="Path 10564" d="M7 0v14" />

      <path data-name="Path 10565" d="M14 7H0" />
    </g>
  </svg>
);

export default PlusIcon;
