import React from 'react';

const MinusIcon = ({ width, height, stroke, fill }) => (
  <svg width={width} height={height} stroke={stroke} fill={fill}>
    <path data-name="Rectangle 12077" d="M0 0h10v2H0z" />
  </svg>
);

export default MinusIcon;
