import React from 'react';

import styled from 'styled-components';

import { BasketConsumer } from '../../providers/BasketProvider';

const Basket = () => {
  // const { prices, loadingPrice } = useBasketContext();
  return (
    <>
      <BasketConsumer>
        {({ basket }) => {
          console.log(basket);
          return (
            <ul>
              {basket &&
                basket.map((line) => {
                  const { product, quantity, product_id: id } = line;
                  const { description_short: descriptionShort, image_small: thumb } = product;
                  return (
                    <li>
                      <p>
                        <img src={thumb} alt="" />
                        Product: {descriptionShort}
                      </p>
                      <p>Quantity: {quantity}</p>
                    </li>
                  );
                })}
            </ul>
          );
        }}
      </BasketConsumer>
    </>
  );
};
export default Basket;
