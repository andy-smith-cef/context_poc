import React from 'react';

const BasketIcon = ({ height, width, stroke }) => (
  <svg width={width} height={height} viewBox="0 0 20.903 19" stroke={stroke}>
    <g data-name="Group 607" transform="translate(-171.938 -662.5)" fill="none">
      <path data-name="Path 1" d="M171.938 663h2.773l3.62 13.961h11.375" />
      <circle
        data-name="Ellipse 1"
        cx={1.384}
        cy={1.384}
        transform="translate(177.464 678.233)"
        r={1.384}
      />
      <circle
        data-name="Ellipse 2"
        cx={1.384}
        cy={1.384}
        transform="translate(187.083 678.233)"
        r={1.384}
      />
      <path data-name="Path 2" d="M175.295 665.252h16.9l-2.347 9.171h-12.175" />
    </g>
  </svg>
);

export default BasketIcon;
