import React, { useState, Fragment, useRef } from 'react';
import styled from 'styled-components';

import { BasketConsumer } from '../../providers/BasketProvider';
import Colors from '../../constants/Colors';
import Basket from './assets/BasketIcon';
import { CefText } from '../../components/styles/Typography';

const iconConfig = {
  stroke: Colors.fontMain,
  height: 21,
  width: 21,
};

const BasketPreview = () => (
  <BasketConsumer>
    {({ basketCount }) => {
      return (
        <BasketPreviewContainer onClick={() => (window.location.href = '/basket')}>
          <Basket {...iconConfig} />
          {basketCount > 0 && (
            <BasketIconWrapper>
              <IconText color={Colors.white} bold>
                {basketCount}
              </IconText>
            </BasketIconWrapper>
          )}
        </BasketPreviewContainer>
      );
    }}
  </BasketConsumer>
);

export default BasketPreview;

const BasketPreviewContainer = styled.div`
  position: relative;
  width: 16px;
  height: 16px;
  margin-bottom: 30px;
`;

const IconText = styled(CefText)`
  font-size: 10px;
`;

export const BasketIconWrapper = styled.div`
  align-tems: center;
  background-color: ${Colors.cefRed};
  border-radius: 8px;
  height: 16px;
  width: 16px;
  padding: 0 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  right: -10px;
  top: -3px;
`;
