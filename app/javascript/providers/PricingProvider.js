import React, { useState, useEffect, useContext } from 'react';

import Axios from 'axios';

export const PricingContext = React.createContext();
export const PricingConsumer = PricingContext.Consumer;

const PricingProvider = ({ children }) => {
  const [prices, setPrices] = useState({});
  const [loadingPrice, setLoadingPrice] = useState(true);

  const {
    productPrices,
    setProductsForPricing,
    setProductPriceUrl,
    setSinglePriceCheck,
  } = usePricing();

  return <PricingContext.Provider value={{}}>{children}</PricingContext.Provider>;
};

export default PricingProvider;

export const usePricingContext = () => useContext(PricingContext);
