import React, { useState, useEffect, useContext } from 'react';

import { getBasket } from '../utils/common.js';
import { validateBasket } from '../utils/validateBasket';
import { asyncLocalStorage } from '../utils/awaitAsyncLocalStorage';
import usePricing from '../hooks/usePricing';
import Axios from 'axios';

export const BasketContext = React.createContext();
export const BasketConsumer = BasketContext.Consumer;

const BasketProvider = ({ children }) => {
  const [basket, setBasket] = useState(null);
  const [basketCount, setBasketCount] = useState(0);
  const [basketTotals, setBasketTotals] = useState({
    subTotal: Number(0).toFixed(2),
    total: Number(0).toFixed(2),
    vat: Number(0).toFixed(2),
  });
  const [customer, setCustomer] = useState({
    id: '',
    deliveryAddress: {},
  });
  const [prices, setPrices] = useState({});
  const [loadingPrice, setLoadingPrice] = useState(true);
  const { productPrices, setProductsForPricing, setSinglePriceCheck } = usePricing();

  const updateBasket = async (id, quantity, product, isBasket = false) => {
    let basketString = await getBasket();
    let parsedBasket = JSON.parse(basketString);

    if (basketString !== null) {
      const index = parsedBasket.findIndex((b) => b.product_id === id);

      if (index === -1) {
        parsedBasket.push({ product_id: id, quantity, product });
        asyncLocalStorage.setItem('@basket', JSON.stringify(parsedBasket));
        handleBasketValidation(parsedBasket);

        if (isBasket) {
          const productPriceParams = {
            [id]: quantity,
          };

          setSinglePriceCheck(true);
          setLoadingPrice(productPriceParams);
          setProductsForPricingContext(productPriceParams);
        }
      } else {
        isBasket
          ? (parsedBasket[index].quantity = quantity)
          : (parsedBasket[index].quantity = parsedBasket[index].quantity + quantity);
        asyncLocalStorage.setItem('@basket', JSON.stringify(parsedBasket));
        await handleBasketValidation(parsedBasket);
        if (isBasket) {
          const productPriceParams = {
            [id]: parsedBasket[index].quantity,
          };

          setSinglePriceCheck(true);
          setLoadingPrice(productPriceParams);
          setProductsForPricingContext(productPriceParams);
        }
      }
    } else {
      asyncLocalStorage.setItem('@basket', JSON.stringify([{ product_id: id, quantity, product }]));
      basketString = await getBasket();
      parsedBasket = JSON.parse(basketString);
      handleBasketValidation(parsedBasket);
      if (isBasket) {
        const productPriceParams = {
          [id]: quantity,
        };
        setLoadingPrice(productPriceParams);
        setSinglePriceCheck(true);
        setProductsForPricingContext(productPriceParams);
      }
    }
  };

  const removeItem = (id) => {
    const index = basket.findIndex((b) => b.product_id === id);
    basket.splice(index, 1);

    if (JSON.stringify(basket).length === 2) {
      asyncLocalStorage.removeItem('@basket');
      setBasket(null);
      setBasketCount(0);
      setBasketTotals({
        subTotal: Number(0).toFixed(2),
        total: Number(0).toFixed(2),
        vat: Number(0).toFixed(2),
      });
    } else {
      asyncLocalStorage.setItem('@basket', JSON.stringify(basket));
      handleBasketValidation(basket);
    }
  };

  const clearBasket = async () => {
    asyncLocalStorage.removeItem('@basket');
    setBasket(null);
    setBasketCount(0);
    setBasketTotals({
      subTotal: Number(0).toFixed(2),
      total: Number(0).toFixed(2),
      vat: Number(0).toFixed(2),
    });
  };

  const handleBasketValidation = async (parsedBasket) => {
    // const { validateBasketUrl } = config.urls;

    const { id: customerId } = customer;
    const basketLinesResponse = [];

    parsedBasket.map(({ product_id: id, quantity }) => {
      basketLinesResponse.push({
        product_id: id,
        quantity,
      });
    });

    const { basket, removed, subTotal, total, vat } = await validateBasket(
      parsedBasket,
      customerId,
      basketLinesResponse,
    );

    // TODO
    const basketCount = parsedBasket.length || 0;

    asyncLocalStorage.setItem('@basket', JSON.stringify(parsedBasket));

    // if (removed.length > 0) {
    //   const products = removed.map(({ product }) => product.brand_and_description_short);
    //   const title = products.length === 1 ? 'Product' : `${products.length} products`;

    //   // Alert.alert(`${title} removed from basket`, `${products}`, [{ text: 'OK' }]);
    // }
    setBasket(parsedBasket);
    // TODO
    setBasketCount(basketCount);
    setBasketTotals({
      subTotal,
      total,
      vat,
    });
  };

  const setProductsForPricingContext = async (products) => {
    // const { productPriceUrl } = config.urls;
    // setProductPriceUrl(productPriceUrl);
    setProductsForPricing(products);
  };

  useEffect(() => {
    // Listen for config change and only go to validate basket once it has been set

    const basketString = getBasket();
    basketString.then((bs) => {
      if (bs !== null && bs.length === 2) {
        asyncLocalStorage.removeItem('@basket');
      }
      if (bs !== null) {
        const basket = JSON.parse(bs);

        setProductsForPricing(basket);
        handleBasketValidation(basket);
      }
    });
  }, []);

  useEffect(() => {
    if (productPrices.result !== undefined) {
      const { result } = productPrices;

      const mergedPrices = { ...prices, ...result.data };
      setLoadingPrice(false);
      setPrices(mergedPrices);
    }
  }, [productPrices.result]);

  return (
    <BasketContext.Provider
      value={{
        // States
        setCustomer,
        basket,
        setBasket,
        basketCount,
        setBasketCount,
        basketTotals,
        setBasketTotals,
        loadingPrice,
        setLoadingPrice,
        prices,

        // Functions
        updateBasket,
        removeItem,
        clearBasket,
        handleBasketValidation,
        setProductsForPricingContext,
      }}>
      {children}
    </BasketContext.Provider>
  );
};

export default BasketProvider;

export const useBasketContext = () => useContext(BasketContext);
